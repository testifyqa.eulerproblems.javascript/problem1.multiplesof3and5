

function findSumOfMultiples(maxNum) {
  let sum = 0;
  for(let i = 1; i < maxNum; i++) {
    if(i%3 == 0 || i%5 == 0) {
      sum = sum+i;
    }
  }
  console.log(sum);
  return sum;
}

module.exports = findSumOfMultiples;